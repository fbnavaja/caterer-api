db.createCollection('caterer');
db.createCollection('events');
db.createUser(
  {
    user: 'api_user',
    pwd: '$2a$12$78i/mYwh.0jhClek1rt4gen5UawRLOCBnYzOmBhZEAL8CVGZx/j0W',
    roles: [{ role: 'readWrite', db: 'caterer-api' }],
  },
);